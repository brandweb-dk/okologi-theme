


<?php
if ( is_page() && $post->post_parent > 0  ) {
    echo '<div class="col-md-8">';
    if ( have_posts() ) {
    	while ( have_posts() ) {
    		the_post();
    		//
    		the_content();
    		//
    	} // end while
    } // end if
    echo '</div>';
    echo '<div class="col-md-3 col-md-offset-1">';
    echo dynamic_sidebar('sidebar-primary');
    echo '</div>';
}
else {
    echo '<div class="col-md-10 col-md-offset-1">';
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            //
            the_content();
            //
        } // end while
    } // end if
    echo '</div>';
}
?>
