<footer class="content-info">
  <div class="container">
    <div class="row vertical-align">
      <div class="col-md-6"></div>
      <div class="col-md-2">
        <img class="img-responsive img-center" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/okomaerke.png" alt="" />
      </div>

      <div class="col-md-2">
        <img class="img-responsive img-center" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/eu-oko.png" alt="" />
      </div>

      <div class="col-md-2">
        <img class="img-responsive img-center" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/knuthenlund-logo.png" alt="" />
      </div>
      <div class="col-md-6"></div>
    </div>
  </div>
</footer>
