<?php
/**
 * Template Name: Sidebar - lokale producenter
 */
?>

<div class="col-md-8">
  <?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page'); ?>
  <?php endwhile; ?>
</div>

<div class="col-md-3 col-md-offset-1">
  <?php dynamic_sidebar('sidebar-primary'); ?>
</div>
